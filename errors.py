class IncompatibleArgumentsException(Exception):
    def __init__(self, arg1, arg2):
        self.arg1 = arg1
        self.arg2 = arg2

    def __str__(self):
        return "%s, %s" % (str(type((self.arg1))), str(type(self.arg2)))


class UndefinedVariableException(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Undefined variable '%s'." % self.name


class UnexpectedBreakException(Exception):
    def __init__(self):
        pass

    def __str__(self):
        return "Slowko kluczowe `break` moze zostac uzyte tylko wewnatrz petli for `lub` `while`."


class MethodAlreadyDefinedException(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Metoda '%s' zostala juz zdefiniowana." % self.name


class UndefinedMethodException(Exception):
    def __init__(self, name, argslen):
        self.name = name
        self.argslen = argslen

    def __str__(self):
        return "Niezdefiniowana metoda '%s'(%s argumentow)." % (self.name, str(self.argslen))


class IterationByZeroException(Exception):
    def __init__(self):
        pass

    def __str__(self):
        return "Nie mozna iterowac o 0."


class InvalidCurlyBracketsException(Exception):
    def __init__(self):
        pass

    def __str__(self):
        return "Brak nawiasu wasatego do zamkniecia."


class IndexOutOfBounds(Exception):
    def __init__(self, name, index):
        self.name = name
        self.index = index

    def __str__(self):
        return "Odwolanie do nieistniejacego elementu tablicy %s[%s]" % (self.name, str(self.index))


class NotListException(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Zmienna %s nie jest lista" % self.name


class NotIndexException(Exception):
    def __init__(self, index):
        self.index = index

    def __str__(self):
        return "Indeks %s nie jest prawidlowym indeksem" % self.index
