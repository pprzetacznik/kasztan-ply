# -*- coding: utf-8 -*-
import sys
from cmd import Cmd
import ply.lex as lex
import ply.yacc as yacc

from ast import *

'''Tokeny'''
reserved = {
    'print': 'PRINT',
    'if': 'IF',
    'else': 'ELSE',
    'while': 'WHILE',
    'for': 'FOR',
    'from': 'FROM',
    'to': 'TO',
    'by': 'BY',
    'break': 'BREAK',
    'function': 'FUNCTION',
    'return': 'RETURN',
    'false': 'FALSE',
    'true': 'TRUE',
    'kasztan': 'KASZTAN',
    'size': 'SIZE',
}
tokens = [
             'ADD', 'SUB', 'MUL', 'DIV', 'MOD', # +, -, *, /, %
             'EQ', 'NEQ', 'LT', 'GT', 'LEQ', 'GEQ', # ==, !=, <, >, <=, >=
             'ASSIGN', 'LP', 'RP', 'LC', 'RC', 'LS', 'RS', 'COMMA', # =, (, ), {, }, [, ], ,
             'VARIABLE', 'INT', 'FLOAT', 'STRING', # abc, 9, -2.3, "asd"
             'SEMICOLON', # ;
         ] + list(reserved.values())

'''Priorytety operatorów'''
precedence = (
    ("nonassoc", 'EQ', 'NEQ', 'LT', 'GT', 'LEQ', 'GEQ'),
    # ("right", 'LS'),
    # ("left", 'RS'),
    ("left", 'ADD', 'SUB'),
    ("left", 'MUL', 'DIV', 'MOD')
)

'''Leksemy'''
#Ignorowane symbole
t_ignore = ' \t'
#Operatory porównania
t_EQ = '=='
t_NEQ = '!='
t_LT = '<'
t_GT = '>'
t_LEQ = '<='
t_GEQ = '>='
#Operatory arytmetyczne
t_ADD = '\+'
t_SUB = '-'
t_MUL = "\*"
t_DIV = '/'
t_MOD = '%'
#Symbole
t_LP = '\('
t_RP = '\)'
t_LC = '{'
t_RC = '}'
t_LS = '\['
t_RS = '\]'
t_ASSIGN = '='
t_COMMA = ','
#Koniec linii
t_SEMICOLON = ';'


def t_FALSE(t):
    r'false'
    t.type = reserved.get(t.value, 'BOOL')
    t.value = False
    return t


def t_TRUE(t):
    r'true'
    t.type = reserved.get(t.value, 'BOOL')
    t.value = True
    return t


def t_VARIABLE(t):
    r'[a-zA-Z][a-zA-Z0-9]*'
    t.type = reserved.get(t.value, 'VARIABLE')
    return t


def t_FLOAT(t):
    r'-?\d+\.\d+'
    t.value = float(t.value)
    return t


def t_INT(t):
    r'-?\d+'
    t.value = int(t.value)
    return t


def t_STRING(t):
    r'"[^"]*"'
    t.value = t.value[1:-1]
    return t


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


'''Struktury i zmienne pomocnicze'''
#Zmienne globalne

'''Gramatyka'''


def p_start_all(p):
    """start : body"""
    p[0] = p[1]
    p[0].parent = None


def p_body_multi(p):
    """body : body line"""
    body = Body()
    body.add(p[1].body)
    body.add([p[2]])
    p[0] = body


def p_body_single(p):
    """body : line"""
    body = Body()
    body.add([p[1]])
    p[0] = body


def p_body_none(p):
    """body : """
    p[0] = Body()


def p_line(p):
    """line : print SEMICOLON
            | assignment SEMICOLON
            | if SEMICOLON
            | while SEMICOLON
            | for SEMICOLON
            | function SEMICOLON
            | call SEMICOLON
            | break SEMICOLON
            | kasztan SEMICOLON"""
    p[0] = p[1]


def p_kasztan(p):
    """kasztan : KASZTAN"""
    print(" _                          _                   ")
    print("| | __   __ _   ___   ____ | |_    __ _   _ __  ")
    print("| |/ /  / _` | / __| |_  / | __|  / _` | | '_ \ ")
    print("|   <  | (_| | \__ \  / /  | |_  | (_| | | | | |")
    print("|_|\_\  \__,_| |___/ /___|  \__|  \__,_| |_| |_|")
    p[0] = Statement()


def p_break(p):
    """break : BREAK"""
    p[0] = Break()

#variables
def p_variables_multi(p):
    """variables : variablesmore"""
    p[0] = p[1]


def p_variables_none(p):
    """variables : """
    p[0] = []


def p_variables_more_multi(p):
    """variablesmore : variablesmore COMMA VARIABLE"""
    p[0] = p[1] + [p[3]]


def p_variables_more_single(p):
    """variablesmore : VARIABLE"""
    p[0] = [p[1]]

#function
def p_function(p):
    """function : FUNCTION VARIABLE LP variables RP LC body return RC"""
    p[0] = Function(p[2], p[4], p[7], p[8])
    p[7].parent = p[0]


def p_return(p):
    """return : RETURN expression SEMICOLON"""
    p[0] = p[2]

#for
def p_for(p):
    """for : FOR VARIABLE FROM expression TO expression BY INT LC body RC"""
    variable = Variable(p[2])
    start = p[4]
    end = p[6]
    by = Value(p[8])
    if by.evaluate() == 0:
        raise IterationByZeroException()

    operator = "<="
    if by.evaluate() < 0:
        operator = ">="
    condition = Condition(variable, operator, end)
    end.parent = condition
    assignment = Assignment(variable, start)
    start.parent = assignment
    expression = Assignment(variable, Expression(variable, "+", by))
    p[0] = For(variable, assignment, expression, condition, p[10])
    assignment.parent = p[0]
    expression.parent = p[0]
    condition.parent = p[0]
    p[10].parent = p[0]


def p_for_by_one(p):
    """for : FOR VARIABLE FROM expression TO expression LC body RC"""
    variable = Variable(p[2])
    start = p[4]
    end = p[6]
    by = Value(1)
    operator = "<="
    condition = Condition(variable, operator, end)
    end.parent = condition
    assignment = Assignment(variable, start)
    start.parent = assignment
    expression = Assignment(variable, Expression(variable, "+", by))
    p[0] = For(variable, assignment, expression, condition, p[8])
    assignment.parent = p[0]
    expression.parent = p[0]
    condition.parent = p[0]
    p[8].parent = p[0]

#while
def p_while(p):
    """while : WHILE condition LC body RC"""
    p[0] = While(p[2], p[4])
    p[2].parent = p[0]
    p[4].parent = p[0]

#print
def p_print(p):
    """print : PRINT expression"""
    p[0] = Print(p[2])
    p[2].parent = p[0]

#if	
def p_if_else(p):
    """if : IF condition LC body RC ELSE LC body RC"""
    p[0] = If(p[2], p[4], p[8])
    p[2].parent = p[0]
    p[4].parent = p[0]
    p[8].parent = p[0]


def p_if(p):
    """if : IF condition LC body RC"""
    p[0] = If(p[2], p[4], Body())
    p[2].parent = p[0]
    p[4].parent = p[0]

#assignment
def p_assignment(p):
    """assignment : VARIABLE ASSIGN expression"""
    variable = Variable(p[1])
    p[0] = Assignment(variable, p[3])
    p[3].parent = p[0]

#parameters
def p_parameters_multi(p):
    """parameters : parametersmore"""
    p[0] = p[1]


def p_parameters_none(p):
    """parameters : """
    p[0] = []


def p_parameters_more_multi(p):
    """parametersmore : parametersmore COMMA expression"""
    p[0] = p[1] + [p[3]]


def p_parameters_more_single(p):
    """parametersmore : expression"""
    p[0] = [p[1]]

#expression
def p_expression_call(p):
    """expression : call"""
    p[0] = p[1]


def p_call(p):
    """call : VARIABLE LP parameters RP"""
    p[0] = Call(p[1], p[3])


#list
def p_expression_getelementfromlist(p):
    """expression : getelementfromlist"""
    p[0] = p[1]


def p_getelementfromlist(p):
    """getelementfromlist : VARIABLE LS VARIABLE RS
                        | VARIABLE LS INT RS"""
    p[0] = ElementFromList(p[1], p[3])


def p_expression_getlistsize(p):
    """expression : getlistsize"""
    p[0] = p[1]


def p_getlistsize(p):
    """getlistsize : SIZE LP VARIABLE RP"""
    p[0] = ListSize(p[3])


def p_expression_value(p):
    """expression : INT
                  | FLOAT
                  | STRING
                  | TRUE
                  | FALSE"""
    p[0] = Value(p[1])


def p_expression_variable(p):
    """expression : VARIABLE"""
    p[0] = Variable(p[1])


def p_expression_condition(p):
    """expression : condition"""
    p[0] = p[1]
    p[1].parent = p[0]


def p_expression_arithmetic(p):
    """expression : expression ADD expression
                  | expression SUB expression
                  | expression MUL expression
                  | expression DIV expression
                  | expression MOD expression"""
    if p[2] == "+":
        p[0] = Expression(p[1], "+", p[3])
    elif p[2] == "-":
        p[0] = Expression(p[1], "-", p[3])
    elif p[2] == "*":
        p[0] = Expression(p[1], "*", p[3])
    elif p[2] == "/":
        p[0] = Expression(p[1], "/", p[3])
    elif p[2] == "%":
        p[0] = Expression(p[1], "%", p[3])
    p[1].parent = p[0]
    p[3].parent = p[0]


def p_expression_parentheses(p):
    """expression : LP expression RP"""
    p[0] = p[2]
    p[2].parent = p[0]


#list
def p_expression_list(p):
    """expression : list"""
    p[0] = p[1]


def p_list(p):
    """list : LS parameters RS"""
    p[0] = MyList(p[2])


#condition
def p_condition_comparison(p):
    """condition : LP expression EQ expression RP
                 | LP expression NEQ expression RP
                 | LP expression LT expression RP
                 | LP expression GT expression RP
                 | LP expression LEQ expression RP
                 | LP expression GEQ expression RP"""
    if p[3] == "==":
        p[0] = Condition(p[2], "==", p[4])
    elif p[3] == "!=":
        p[0] = Condition(p[2], "!=", p[4])
    elif p[3] == "<":
        p[0] = Condition(p[2], "<", p[4])
    elif p[3] == ">":
        p[0] = Condition(p[2], ">", p[4])
    elif p[3] == "<=":
        p[0] = Condition(p[2], "<=", p[4])
    elif p[3] == ">=":
        p[0] = Condition(p[2], ">=", p[4])
    p[2].parent = p[0]
    p[4].parent = p[0]


'''Błędy'''
#Błąd parsowania (analizy syntaktycznej)
def p_error(p):
    print u"Blad skladniowy w linii '%d'." % p.lineno

#Błąd skanowania (analizy leksykalnej)
def t_error(t):
    print u"Nie rozpoznano znaku '%s'." % t.value[0]
    t.lexer.skip(1)


'''Buduj lexer i skaner'''
lexer = lex.lex()
parser = yacc.yacc()

'''Załaduj plik'''

if len(sys.argv) == 2:
    file = None
    try:
        file = open(sys.argv[1], "r")
    except:
        print u"Nie mozna otworzyc pliku '%s'." % sys.argv[1]
    sourceCode = file.read()
    body = parser.parse(sourceCode, lexer=lexer)
    body.evaluate()
else:
    cmd = Cmd()
    body = Body()
    cmd.buildline = ""
    cmd.openbrackets = 0

    def read(line):
        cmd.buildline += line
        cmd.openbrackets = 0
        cmd.instring = False
        for char in cmd.buildline:
            if char == '"':
                cmd.instring = not cmd.instring
            elif char == '{' and not cmd.instring:
                cmd.openbrackets += 1
            elif char == '}' and not cmd.instring:
                if cmd.openbrackets > 0:
                    cmd.openbrackets -= 1
                else:
                    raise InvalidCurlyBracketsException()
        if (cmd.openbrackets == 0 and line.rstrip().endswith(";")):
            newBody = parser.parse(cmd.buildline, lexer=lexer)
            cmd.buildline = ""
            body.add(newBody.body)
            body.evaluate()

    def emptyline():
        pass

    def exit(line):
        print "Czesc!"
        return True

    cmd.default = read
    cmd.emptyline = emptyline
    cmd.do_exit = exit
    cmd.prompt = "> "
    cmd.postcmd(True, "exit")
    cmd.cmdloop("Kasztan!")
