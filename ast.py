from errors import *

functions = {}


class Statement:
    def __init__(self):
        self.evaluated = False
        pass

    def evaluate(self):
        pass


class Body(Statement):
    def __init__(self):
        self.evaluated = False
        self.parent = None
        self.body = []
        self.environment = {}

    def evaluate(self):
        for statement in self.body:
            if not statement.evaluated:
                if self.parent == None:
                    statement.evaluated = True
                statement.evaluate()

    def add(self, body):
        self.body += body
        for statement in self.body:
            statement.parent = self

    def get(self, name):
        if name in self.environment:
            return self.environment[name]
        elif self.parent is not None:
            return self.parent.get(name)
        else:
            return None

    def set(self, name, expression):
        if self.parent is not None:
            value = self.parent.get(name)
            if value is not None:
                self.parent.set(name, expression)
            else:
                self.environment[name] = expression.evaluate()
        else:
            self.environment[name] = expression.evaluate()


class Expression(Statement):
    def __init__(self, arg1, operator, arg2):
        self.parent = None
        self.arg1 = arg1
        self.operator = operator
        self.arg2 = arg2

    def evaluate(self):
        eval1 = self.arg1.evaluate()
        eval2 = self.arg2.evaluate()
        if self.operator == "+":
            return eval1 + eval2
        elif self.operator == "-":
            return eval1 - eval2
        elif self.operator == "*":
            return eval1 * eval2
        elif self.operator == "/":
            return eval1 / eval2
        elif self.operator == "%":
            return eval1 % eval2

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        self.parent.set(name, expression)


class Variables(Statement):
    def __init__(self, variables):
        self.parent = None
        self.variables = variables


class Parameters(Statement):
    def __init__(self, parameters):
        self.parent = None
        self.parameters = parameters


class Function(Statement):
    def __init__(self, name, variables, body, returnExpression):
        self.evaluated = False
        self.parent = None
        self.name = name
        self.variables = variables
        self.body = body
        self.returnExpression = returnExpression

    def evaluate(self):
        if self.name in functions:
            raise MethodAlreadyDefinedException(self.name)
        functions[self.name] = self

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        self.parent.set(name, expression)


class Call(Statement):
    def __init__(self, name, parameters):
        self.evaluated = False
        self.parent = None
        self.name = name
        self.parameters = parameters
        for parameter in self.parameters:
            parameter.parent = self
        self.function = None

    def evaluate(self):
        if self.name not in functions:
            raise UndefinedMethodException(self.name, len(self.parameters))
        self.function = functions[self.name]
        if len(self.function.variables) != len(self.parameters):
            raise UndefinedMethodException(self.name, len(self.parameters))
        self.function.parent = self #?!
        for name in self.function.variables:
            self.function.body.environment[name] = self.parameters[self.function.variables.index(name)].evaluate()
        self.function.body.evaluate()
        self.function.returnExpression.parent = self.function.body
        return self.function.returnExpression.evaluate()

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        if name in self.function.variables:
            index = self.function.variables.index(name)
            self.parameters[index] = expression.evaluate()
        else:
            self.parent.set(name, expression)


class ElementFromList(Statement):
    def __init__(self, name, index):
        self.evaluated = False
        self.parent = None
        self.name = name
        self.index = index

    def evaluate(self):
        table = self.parent.get(self.name)
        index = self.index
        if not isinstance(index, int):
            index = self.parent.get(self.index)
            if not isinstance(index, int):
                raise NotIndexException(self.index)
        size = len(table)
        if index >= size or index < 0:
            raise IndexOutOfBounds(self.name, index)
        return table[index]


class ListSize(Statement):
    def __init__(self, name):
        self.evaluated = False
        self.parent = None
        self.name = name

    def evaluate(self):
        variable = self.parent.get(self.name)
        if isinstance(variable, list):
            return len(variable)
        raise NotListException(self.name)


class Break(Statement):
    def __init__(self):
        self.evaluated = False
        self.parent = None

    def evaluate(self):
        parent = self
        correct = False
        while parent is not None and correct == False:
            if isinstance(parent, For) or isinstance(parent, While):
                correct = True
                parent.breakLoop()
            parent = parent.parent
        if correct == False:
            raise UnexpectedBreakException()


class For(Statement):
    def __init__(self, variable, assignment, expression, condition, body):
        self.evaluated = False
        self.parent = None
        self.variable = variable
        self.value = None
        self.assignment = assignment
        self.expression = expression
        self.condition = condition
        self.body = body
        self.broken = False

    def breakLoop(self):
        self.broken = True

    def evaluate(self):
        self.broken = False
        self.assignment.evaluate()
        while self.condition.evaluate() and self.broken == False:
            self.evaluateBody(self.body.body)
            if self.broken == False:
                self.expression.evaluate()

    def evaluateBody(self, body):
        for statement in body:
            if isinstance(statement, Body):
                self.evaluateBody(statement)
            else:
                if self.broken is False:
                    statement.evaluate()
                else:
                    break

    def get(self, name):
        if self.variable.name == name:
            return self.value
        else:
            return self.parent.get(name)

    def set(self, name, expression):
        if self.variable.name == name:
            self.value = expression.evaluate()
        else:
            self.parent.set(name, expression)


class While(Statement):
    def __init__(self, condition, body):
        self.evaluated = False
        self.parent = None
        self.condition = condition
        self.body = body
        self.broken = False

    def breakLoop(self):
        self.broken = True

    def evaluate(self):
        self.broken = False
        while self.condition.evaluate() and self.broken == False:
            self.evaluateBody(self.body.body)

    def evaluateBody(self, body):
        for statement in body:
            if isinstance(statement, Body):
                self.evaluateBody(statement)
            else:
                if self.broken is False:
                    statement.evaluate()
                else:
                    break

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        self.parent.set(name, expression)


class Print(Statement):
    def __init__(self, expression):
        self.evaluated = False
        self.parent = None
        self.expression = expression

    def evaluate(self):
        print self.expression.evaluate()
        # print str(self.expression.evaluate())

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        self.parent.set(name, expression)


class Value(Expression):
    def __init__(self, value):
        self.parent = None
        self.value = value

    def evaluate(self):
        return self.value


class Variable(Expression):
    def __init__(self, name):
        self.parent = None
        self.name = name

    def evaluate(self):
        value = self.parent.get(self.name)
        if value is None:
            raise UndefinedVariableException(self.name)
        return value


class MyList(Expression):
    def __init__(self, value):
        self.parent = None
        self.value = value

    def evaluate(self):
        list = []
        for item in self.value:
            if isinstance(item, Variable):
                list += [self.parent.get(item.name)]
            else:
                list += [item.evaluate()]
        return list


class Assignment(Statement):
    def __init__(self, variable, expression):
        self.evaluated = False
        self.parent = None
        self.variable = variable
        self.expression = expression

    def evaluate(self):
        self.parent.set(self.variable.name, self.expression)
        self.variable.parent = self.parent

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        self.parent.set(name, expression)


class Condition(Expression):
    def __init__(self, arg1, operator, arg2):
        self.parent = None
        self.arg1 = arg1
        self.operator = operator
        self.arg2 = arg2

    def evaluate(self):
        eval1 = self.arg1.evaluate()
        eval2 = self.arg2.evaluate()
        if self.operator == "==":
            return eval1 == eval2
        elif self.operator == "!=":
            return eval1 != eval2
        elif self.operator == ">":
            return eval1 > eval2
        elif self.operator == "<":
            return eval1 < eval2
        elif self.operator == ">=":
            return eval1 >= eval2
        elif self.operator == "<=":
            return eval1 <= eval2

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        self.parent.set(name, expression)


class If(Statement):
    def __init__(self, condition, ifBody, elseBody):
        self.evaluated = False
        self.parent = None
        self.condition = condition
        self.ifBody = ifBody
        self.elseBody = elseBody

    def evaluate(self):
        if self.condition.evaluate():
            self.ifBody.evaluate()
        else:
            self.elseBody.evaluate()

    def get(self, name):
        return self.parent.get(name)

    def set(self, name, expression):
        self.parent.set(name, expression)